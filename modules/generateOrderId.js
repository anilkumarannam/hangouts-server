const generateOrderId = () => {
  const nameCodes = [];
  for (let index = 0; index < 10; index++) {
    nameCodes.push(Math.floor(Math.random() * (10) + 48));
  }
  return String.fromCharCode(...nameCodes);
};

module.exports = generateOrderId;