const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const printLogin = require("./serverModules/printLogin");
const sendMessage = require("./serverModules/sendMessage");
const addUser = require("./serverModules/addUser");
const getFriendsList = require("./serverModules/getFriendsList");
const getUserDetails = require("./serverModules/getUserDetails");
const server = express();
server.use(cors({ origin: true }));
server.use(bodyParser.json()); // to support JSON-encoded bodies
server.use(
 bodyParser.urlencoded({
  // to support URL-encoded bodies
  extended: true,
 })
);

const port = 8000;
let message_id = 1000000;
server.get("/", async (req, res) => {});

server.post("/sendMessage/", async (req, res) => {
 const { body } = req;
 message_id = message_id + 1;
 const senderBody = { message_id, ...body };
 await sendMessage(senderBody);
 res.send({ response: "Message sent successfully..!!!" });
});

server.post("/addUser/", async (req, res) => {
 const { body } = req;
 const userInformation = await addUser(body);
 res.send({ code: 200, response: userInformation, status: "SUCCESS"});
});

server.get("/friends/:userId", async (req, res) => {
 const { userId } = req.params;
 const body = { google_id: userId };
 const list = await getFriendsList(body);
 res.send({ response: list });
});

server.get("/user/:userId", async (req, res) => {
 const { userId } = req.params;
 const list = await getUserDetails(userId);
 res.status(200).send({status: "SUCCESS", code: 200, response: list });
});

server.get("/ping/", async (req, res) => {
 res.send({ response: "pinged" });
});

server.listen(port, () => {
 console.log(`Server running at ${port}`);
});
