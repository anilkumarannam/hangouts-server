const { userTable } = require("../authentication/firestore");
const getUserDetails = async (id) => {
 if(!id) return {};
 const document = await userTable.doc(id).get();
 const responseData = document.data();
 const { name, image_url, email_id } = responseData || {};
 const response = {
   id,
   name: name, 
   image_url: image_url,
   email_id
 }
 return response;
};
module.exports = getUserDetails;
