const { userTable } = require("../authentication/firestore");

const getFriendsList = async (body) => {
 const { google_id } = body;
 const friendsList = await userTable.where("google_id", "!=", google_id).get();
 const response = [];
 if (!friendsList.empty) {
  friendsList.forEach((doc) => {
   response.push(doc.data());
  });
 }
 return response;
};

module.exports = getFriendsList;
