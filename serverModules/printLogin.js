const { login } = require("../authentication/firestore");
const printLogin = async () => {
 const data = await login.get();
 data.forEach((doc) => {
  console.log(doc.id, " => ", doc.data());
 });
};

module.exports = printLogin;
