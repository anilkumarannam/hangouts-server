const { messageTable } = require("../authentication/firestore");

const sendMessage = async (body) => {
 const { sender_id, receiver_id, messageText, message_id } = body;
 await messageTable.doc().set({ message_id, sender_id, receiver_id, messageText });
};

module.exports = sendMessage;
