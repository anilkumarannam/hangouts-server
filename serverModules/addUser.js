const { userTable } = require("../authentication/firestore");

const addUser = async (body) => {
 const { google_id, email_id, name, given_name, image_url, ...rest } = body;

 const existingUser = await userTable.where("google_id", "==", google_id).get();
 const user = { google_id, email_id, name, given_name, image_url, friends: [] };
 if (existingUser.empty) {
  const response = await userTable.add(user);
  const responseData = await response.get();
  const data = responseData.data();

  return {
    id: response.id,
    ...data
  }
  return 
 }else{
   return {
     id: existingUser.docs[0]._ref.id,
     ...existingUser.docs[0].data()
   }
 }
};

module.exports = addUser;
